# The pythondev slack community website

[![build status](https://gitlab.com/PythonDevCommunity/pythondev-site/badges/master/build.svg)](https://gitlab.com/PythonDevCommunity/pythondev-site/commits/master)

A website built for the pythondev community of slack.

Join us: http://pythondevelopers.herokuapp.com/
